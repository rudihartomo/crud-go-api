module shop_api

go 1.15

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/mux v1.7.4
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.3.5
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
)
