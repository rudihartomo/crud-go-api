package models

type User struct {
	ID       interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Username string      `json:"username"`
	Password string      `json:"password"`
}

type UserUpdate struct {
	ModifiedCount int64 `json:"modifiedCount"`
	Result        User
}

type UserDelete struct {
	DeletedCount int64 `json:"deletedCount"`
}
