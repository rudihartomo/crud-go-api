package main

import (
	"context"
	"shop_api/config"
	"shop_api/database"
	"shop_api/handlers/todo_handlers"
	"shop_api/handlers/user_handlers"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

var identityKey = "id"
var client_db *mongo.Client

func main() {
	conf := config.GetConfig()
	ctx := context.TODO()

	db := database.ConnectDB(ctx, conf.Mongo)
	collection := db.Collection(conf.Mongo.Collection)
	collection_user := db.Collection("user")

	client := &database.TodoClient{
		Col: collection,
		Ctx: ctx,
	}

	client_user := &database.UserClient{
		Col: collection_user,
		Ctx: ctx,
	}

	r := gin.Default()

	todos := r.Group("/todos")
	todos.Use(Authorization(conf.Token))
	{
		todos.GET("/", todo_handlers.SearchTodos(client))
		todos.GET("/:id", todo_handlers.GetTodo(client))
		todos.POST("/", todo_handlers.InsertTodo(client))
		todos.PATCH("/:id", todo_handlers.UpdateTodo(client))
		todos.DELETE("/:id", todo_handlers.DeleteTodo(client))
	}

	users := r.Group("/users")

	users.POST("/signup", user_handlers.UserSignup(client_user))

	r.Run(":8080")
}

func Authorization(token string) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := c.GetHeader("Authorization")
		if token != auth {
			c.AbortWithStatusJSON(401, gin.H{"message": "Invalid authorization token"})
			return
		}
		c.Next()
	}
}
