package user_handlers

import (
	"log"
	"net/http"
	"shop_api/database"
	"shop_api/models"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

var mySigningKey = []byte("captainjacksparrowsayshi")

const (
	APP_KEY = "AAAAA"
)

func getHash(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}

func GenerateJWT(userid string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": userid,
		"exp":  time.Now().Add(time.Hour * time.Duration(1)).Unix(),
		"iat":  time.Now().Unix(),
	})
	tokenString, err := token.SignedString([]byte(APP_KEY))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func UserSignup(db database.UserInterface) gin.HandlerFunc {
	return func(c *gin.Context) {
		user := models.User{}
		err := c.BindJSON(&user)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}
		user.Password = getHash([]byte(user.Password))
		res, err := db.Insert(user)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}
		token, err := GenerateJWT(res.Username)
		c.JSON(http.StatusOK, gin.H{"token": token})
	}
}
